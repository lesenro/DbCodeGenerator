﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DbCodeGenerator
{
   public  class CodeTools
    {
        /// <summary>
        /// 获取名称规则
        /// </summary>
        /// <param name="ntype">1:大驼峰,2:小驼峰,3:全大写,4:全小写,其他不变</param>
        /// <param name="name"></param>
        /// <returns></returns>

        public string humpNaming(int ntype, string name)
        {
            if (ntype == 0)
            {
                return name;
            }
            name = name.ToLower();
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            string[] nameArr = name.Split("-_".ToCharArray());
            if (ntype == 1)//大驼峰
            {
                StringBuilder sb = new StringBuilder();
                foreach (string n in nameArr)
                {
                    sb.Append(textInfo.ToTitleCase(n));
                }
                return sb.ToString();
            }
            else if (ntype == 2)//小驼峰
            {
                bool isFirst = true;
                StringBuilder sb = new StringBuilder();
                foreach (string n in nameArr)
                {
                    if (isFirst)
                    {
                        isFirst = false;
                        sb.Append(textInfo.ToLower(n));
                    }
                    else {
                        sb.Append(textInfo.ToTitleCase(n));
                    }
                }
                return sb.ToString();
            }
            else if (ntype == 3)//全大写
            {
                StringBuilder sb = new StringBuilder();
                foreach (string n in nameArr)
                {
                    sb.Append(textInfo.ToUpper(n));
                }
                return sb.ToString();
            }
            else if (ntype == 4)//全小写
            {
                StringBuilder sb = new StringBuilder();
                foreach (string n in nameArr)
                {
                    sb.Append(textInfo.ToLower(n));
                }
                return sb.ToString();
            }
            else
            {
                return name;
            }
        }
    }
}
