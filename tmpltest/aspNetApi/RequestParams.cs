﻿@{
	var appcfg=Model.dcCfgs.appConfigs;
}using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace @(appcfg["appName"]).Utilities
{
    public class RequestParams
    {
        public string columns { get; set; } = "";
        public int limit { get; set; } = 10;
        public int offset { get; set; } = 0;
        public int pagenum { get; set; } = 1;
        public string orderby { get; set; } = "";
        public string query { get; set; } = "";
    }
}