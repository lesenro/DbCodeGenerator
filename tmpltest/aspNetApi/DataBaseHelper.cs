﻿@{
	var appcfg=Model.dcCfgs.appConfigs;
}using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Web;

namespace @(appcfg["appName"]).Utilities
{
    public class DataBaseHelper<TP> where TP : DbContext
    {
        public TP dbCxt { get; set; }
        public DataBaseHelper(TP db)
        {
            dbCxt = db;
        }
        public ListResult GetList<T>(RequestParams reqParams) where T : class
        {
            IQueryable list;
            int count = 0;
            list = dbCxt.Set(typeof(T));
            if (!string.IsNullOrEmpty(reqParams.query))
            {
                list = list.Where(reqParams.query);
                count = dbCxt.Set(typeof(T)).Where(reqParams.query).Count();
            }
            else
            {
                count = dbCxt.Set(typeof(T)).Count();
            }
            if (!string.IsNullOrEmpty(reqParams.orderby))
            {
                list = list.OrderBy(reqParams.orderby);
            }
			else
            {
                list = list.OrderBy("Id desc");
            }
            if (!string.IsNullOrEmpty(reqParams.columns))
            {
                list = list.Select("New("+reqParams.columns+")");
            }
            list = list.Skip(reqParams.offset).Take(reqParams.limit);
            return new ListResult { List = list, Count = count };
        }
        public ListResult GetList<T>(string query = "", string orderby = "Id desc", int limit = 10, int offset = 0, string columns = "") where T : class
        {
            RequestParams reqParams = new RequestParams {
                columns= columns,
                limit= limit,
                offset= offset,
                orderby = orderby,
                query= query
            };
            return GetList<T>(reqParams);
        }
        public T GetById<T>(long id) where T : class
        {
            var info = dbCxt.Set(typeof(T)).Find(id);

            if (info == null)
            {
                return null;
            }
            else
            {
                return info as T;
            }
        }
        public T Edit<T>(long id, T info) where T : class
        {
            Type t = info.GetType();
            PropertyInfo pinfo = t.GetProperty("Id");

            if (!Exists<T>(id))
            {
                return null;
            }
            try
            {
                var ent = dbCxt.Entry(info);
                ent.State = EntityState.Modified;
                dbCxt.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw ex;
            }
            return info;
        }
        public T Add<T>(T info) where T : class
        {
            dbCxt.Set(typeof(T)).Add(info);
            try
            {
                dbCxt.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return info;
        }
        public T Delete<T>(long id) where T : class
        {
            var info = dbCxt.Set(typeof(T)).Find(id);
            if (info == null)
            {
                return null;
            }

            dbCxt.Set(typeof(T)).Remove(info);
            try
            {
                dbCxt.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return info as T;
        }
        public int MultDelete<T>(int[] ids) where T : class
        {
            try
            {
                string query = "@@0.Contains(outerIt.Id)";
                dbCxt.Set(typeof(T)).RemoveRange(dbCxt.Set(typeof(T)).Where(query, ids));
                return dbCxt.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool Exists<T>(long id)
        {
            return dbCxt.Set(typeof(T)).Where("Id = " + id.ToString()).Count() > 0;
        }
        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbCxt.Dispose();
            }
        }
    }
}