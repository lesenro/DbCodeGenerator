import React from 'react';
import { renderRoutes } from 'react-router-config';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import { AppTools } from '../AppTools';
import { TopNavBar } from './topNavBar'
import { Link } from 'react-router-dom'

const { Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;
function RouteToMenu(item) {
    if (item.props.hidden) {
        return null;
    }
    var title = <span>
        {item.props.icon && <Icon type={item.props.icon} />
        }
        <span>
            {item.props.name}
        </span>
    </span>;

    if (item.routes) {
        var submenu = item
            .routes
            .map(x => {
                return RouteToMenu(x);
            });
        return <SubMenu key={item.path} title={title}>
            {submenu}
        </SubMenu>
    } else {
        return <Menu.Item key={item.path}>{title}</Menu.Item>
    }
}

export class AdminCenter extends React.Component {
    openMenus = [];
    state = {
        collapsed: false,
    };
    onCollapse = (collapsed) => {
        this.setState({ collapsed });
    }
    routeOnChange(routes, thisRoute = false) {
        var thisUrl = this.props.location.pathname;
        var r = null;
        routes.forEach(function (item, idx) {
            
            if (item.path === thisUrl) {
                if (thisRoute) {
                    //当前页面路由
                    r = item;
                    return;
                } else if (item.props && !item.props.hidden) {
                    //非隐藏的路由
                    r = item;
                    return;
                }
            }

            //包含子路由，用于隐藏的路由

            if (item.props && item.props.include && item.props.include.indexOf(thisUrl) !== -1) {
                r = item;
                return;
            }


            if (item.routes) {
                var tmp = this.routeOnChange(item.routes,thisRoute);
                if (tmp) {
                    this.openMenus.push(item.path);
                    r = tmp;
                    return;
                }
            }
        }, this);

        return r;
    }
    render() {
        var routes = this.props.route.routes;
        var defaultItem = this.routeOnChange(routes) || { path: "/admin/home" };
        var thisItem = this.routeOnChange(routes, true);
        if (thisItem && thisItem.props && thisItem.props.title) {
            window.document.title = thisItem.props.title;
        }else{
            thisItem=defaultItem;
        }
        var menus = routes.map(x => {
            if (x.props) {
                return RouteToMenu(x);
            } else {
                return null;
            }
        });
        return (
            <Layout style={{
                minHeight: '100vh'
            }}>
                <Sider
                    collapsible
                    collapsed={this.state.collapsed}
                    onCollapse={this.onCollapse}>
                    <div className="logo" />
                    <Menu
                        theme="dark"
                        onClick={(e) => {
                            AppTools.redirect(e.key);
                        }}
                        defaultSelectedKeys={[defaultItem.path]}
                        defaultOpenKeys={this.openMenus}
                        mode="inline">
                        {menus}
                    </Menu>
                </Sider>
                <Layout>
                    <TopNavBar />
                    <Content
                        style={{
                            margin: '0 16px'
                        }}>
                        <Breadcrumb
                            style={{
                                margin: '12px 0'
                            }}>
                            <Breadcrumb.Item><Link to="/admin/home">首页</Link></Breadcrumb.Item>
                            {defaultItem.path!=="/admin/home"&&defaultItem.path!==thisItem.path&&defaultItem.props&&
                            <Breadcrumb.Item><Link to={defaultItem.path}>{defaultItem.props.name}</Link></Breadcrumb.Item>
                            }
                            {thisItem.path!=="/admin/home"&&thisItem.props&&
                            <Breadcrumb.Item>{thisItem.props.name}</Breadcrumb.Item>
                            }
                        </Breadcrumb>
                        <div
                            style={{
                                padding: 24,
                                background: '#fff',
                                minHeight: 360
                            }}>
                            {renderRoutes(this.props.route.routes)}
                        </div>
                    </Content>
                    <Footer
                        style={{
                            textAlign: 'center'
                        }}>
                        Ant Design ©2016 Created by Ant UED
                    </Footer>
                </Layout>
            </Layout>

        );
    }
}
