import React from 'react';
// import createHasHistory from 'history/createHashHistory'
import {HashRouter as Router} from 'react-router-dom';
import {renderRoutes} from 'react-router-config'
import {routes} from './routes'
//import rootReducer from './reducers/reducers'
// const hasHistory = createHasHistory();

function AppView(props) {
  return <div className="App">
    {props.children}
  </div>
}

//let store = createStore(rootReducer);
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false
    };
  }
  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({collapsed});
  }
  render() {
    return (
        <Router>
          <AppView>
            {renderRoutes(routes)}
          </AppView>
        </Router>
    );
  }
}

export default App;