import createHasHistory from 'history/createHashHistory'
import Cookies from 'js-cookie';
import md5 from 'blueimp-md5';

const hasHistory = createHasHistory();

export class AppTools {
    static redirect(url) {
        hasHistory.push(url);
    };
    //取随机码
    static randCode() {
        return "rnd_" + AppTools.timeStamp();
    };
    //取时间缀
    static timeStamp() {
        return ((new Date()).getTime());
    };
    //取随机码
    static getGuid(seed) {
        seed = seed || AppTools.randCode();
        return md5(seed);
    };
    static setJsonCookie(key, val) {
        var json = JSON.stringify(val);
        Cookies.set(key, json);
    }
    static getJsonCookie(key) {
        var json = Cookies.get(key);
        if (json) {
            return JSON.parse(json);
        } else {
            return null
        }
    }
    static setStringCookie(key, val) {
        Cookies.set(key, val);
    }
    static getStringCookie(key) {
        return Cookies.get(key);
    }
    static getFileBase64 = function(files) {
        var file = files[0];
        var reader = new FileReader();
        var promise = new Promise(function(resolve, reject) {
            reader.onloadend = function(e) {
                resolve(this.result);
            };
        });

        reader.readAsDataURL(file);
        return promise;
    };
    static removeBase64Prefix = function(x) {
        var prefix = "base64,";
        var idx = x.indexOf(prefix);
        return x.substr(idx + prefix.length);
    };
    static queryParams(str) {
        if (!str) {
            return null;
        }
        str = str.replace(/\?/, "");
        var params = str.split("&");
        var result = {};
        params.forEach(function(item) {
            var tmp = item.split("=");
            if (tmp.length > 1) {
                result[tmp[0]] = tmp[1];
            } else {
                result[tmp[0]] = "";
            }
        }, this);
        return result;
    }
}
export class LocalStorageService {
    localStorage = null;
    constructor() {
        if (!localStorage) {
            throw new Error('Current browser does not support Local Storage');
        }
        this.localStorage = localStorage;
    }
    set(key, value) {
        this.localStorage[key] = value;
    }
    get(key) {
        return this.localStorage[key] || false;
    }
    setObject(key, value) {
        this.localStorage[key] = JSON.stringify(value);
    }
    getObject(key) {
        return JSON.parse(this.localStorage[key] || null);
    }
    remove(key) {
        this.localStorage.removeItem(key);
    }
    clearAll() {
        this.localStorage.clear();
    }
}