import React from 'react';
import { Layout, Menu, Icon, notification, Modal } from 'antd';
import { AppTools } from '../AppTools';
import { ApiService } from '../ApiService'
const { Header } = Layout;
const SubMenu = Menu.SubMenu;

export class TopNavBar extends React.Component {
    render() {
        return <Header
            style={{
                background: '#fff',
                padding: 0
            }}>
            <Menu
                className="pull-right"
                mode="horizontal"
                defaultSelectedKeys={['1']}
                onClick={(e) => {
                    switch (e.key) {
                        case "logout":
                            Modal.confirm({
                                title: '退出确认',
                                content: '确定要退出当前用户的登录吗？',
                                onOk() {
                                    ApiService.logout().then(resp => {
                                        if (resp.code === "success") {
                                            notification.success({ message: "登出成功", description: "您已安全登出!" });
                                            AppTools.redirect("/");
                                        }
                                    });
                                },
                                onCancel() {
                                    console.log('Cancel');
                                },
                            });

                            break;
                        default:
                            break;
                    }
                }}
                style={{
                    lineHeight: '63px'
                }}>
                <Menu.Item key="mail">
                    <Icon type="home" />首页
                </Menu.Item>
                <SubMenu className="f-16" title={<span><Icon type="setting" />功能</span>}>
                    <Menu.Item key="modify-password">修改密码</Menu.Item>
                    <Menu.Item key="clear-cathe">清除缓存</Menu.Item>
                    <Menu.Item key="setting:3" disabled={true} style={{height:"1px"}}><hr/></Menu.Item>
                    <Menu.Item key="logout">退出登录</Menu.Item>
                </SubMenu>
            </Menu>
        </Header>
    }
}