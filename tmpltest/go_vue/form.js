@{	
	var appcfg=@Model.dcCfgs.appConfigs;
	var funcName =@Model.codeTools.humpNaming(2,Model.tabname);
	var className =@Model.codeTools.humpNaming(1,Model.tabname);
}
export class @(className)Info {
    constructor() {
        @foreach(var col in Model.tabinfo){
            var colName = Model.codeTools.humpNaming(1,col.columnName);
            if(col.columnType=="string"){
                @Raw("this."+colName+" = \""+col.defaultValue+"\";\r\n")
            }else{
                @Raw("this."+colName+" = "+col.defaultValue+";\r\n")
            }
        }
    }
}
export const @(className)Form = {
    props: {
        "label-width": "120px"
    },
    buttons: {
        hidden: true
    },

    layouts: [
        {
            key: "div",
            name: "div",
            type: "div",
            controls: [
                @foreach(var col in Model.tabinfo){
                    var colName = Model.codeTools.humpNaming(1,col.columnName);
                    <replace>
                    {
                        key: "@colName",
                        name: "@colName",
                        type: "text",
                        itemProps: {
                            rules: [
                                { required: true, message: "请输入 @colName", trigger: "blur" },
                                { min: 0, max: 40, message: "长度在 3 到 40 个字符", trigger: "blur" }
                            ],
                        },
                        controlProps: {
    
                            placeholder: "@colName"
                        }
                    },
                    </replace>
                }                
            ]
        },
    ],
};
