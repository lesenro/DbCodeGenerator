﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbCodeGenerator
{
    public class columnInfo
    {
        public string columnName { get; set; }
        public string columnType { get; set; }
        public bool PrimaryKey { get; set; }
        public ulong size { get; set; }
        public bool autoIncrement { get; set; }
        public string defaultValue { get; set; }
    }
}
